import { Component, OnInit } from '@angular/core';
import { GroupService } from '../shared/group.service';
import { Group } from '../shared/group.model';
@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent implements OnInit {
  groups!: Group[];

  constructor(private groupService: GroupService) {}

  ngOnInit() {
    this.groups = this.groupService.getGroups();
    this.groupService.groupsChange.subscribe((groups: Group[]) => {
      this.groups = groups;
    })
  }

  focusGroup(index: number) {
    this.groups.forEach(current => {
      current.value = false;
    })
    this.groups[index].value = true;
  }
}
