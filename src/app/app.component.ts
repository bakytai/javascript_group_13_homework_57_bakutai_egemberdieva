import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  showCreateComponent = true;
  showUserAndGroups = true;

  changeValue() {
    this.showUserAndGroups = false;
    this.showCreateComponent = true;
  }

  showUsersGroups() {
    this.showCreateComponent = false;
    this.showUserAndGroups = true;
  }
}
