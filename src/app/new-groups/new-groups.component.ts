import { Component, ElementRef, ViewChild } from '@angular/core';
import { GroupService } from '../shared/group.service';
import { Group } from '../shared/group.model';

@Component({
  selector: 'app-new-groups',
  templateUrl: './new-groups.component.html',
  styleUrls: ['./new-groups.component.css']
})
export class NewGroupsComponent{
  @ViewChild('groupNameInput') groupNameInput!: ElementRef;

  constructor(
    public groupService: GroupService,
  ) {}


  createGroup() {
    const groupName = this.groupNameInput.nativeElement.value;
    if (groupName !== '') {
      const group = new Group(groupName);
      this.groupService.addNewGroup(group);
    } else {
      alert('Введите название группы!');
    }
  }
}
