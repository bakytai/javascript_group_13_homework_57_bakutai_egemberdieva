import { EventEmitter } from '@angular/core';
import { Group } from './group.model';


export class GroupService {
  groupsChange = new EventEmitter<Group[]>();
  private groups: Group[] = [
    new Group('Hiking club'),
    new Group('Book club'),
  ];

  getGroups() {
    return this.groups.slice();
  }

  addNewGroup(group: Group) {
      const newGroup = this.groups.some(e => e.name === group.name);
      if (!newGroup) {
        this.groups.push(group);
        this.groupsChange.emit(this.groups);
      } else {
        alert('такая группа уже есть!');
        return
      }
  }


}
