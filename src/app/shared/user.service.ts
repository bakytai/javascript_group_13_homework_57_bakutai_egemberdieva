
import { User } from './user.model';

export class UserService {
  public users: User[] = [
    new User('Andreu', '@bird', true, 'admin'),
    new User('Alan', '@tiger', true, 'admin'),
  ];

}
