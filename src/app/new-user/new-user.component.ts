import { Component, ElementRef, ViewChild } from '@angular/core';
import { UserService } from '../shared/user.service';
import { User } from '../shared/user.model';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent {
  @ViewChild('nameInput') nameInput!: ElementRef;
  @ViewChild('emailInput') emailInput!: ElementRef;
  @ViewChild('checkInput') checkInput!: ElementRef;
  @ViewChild('roleInput') roleInput!: ElementRef;
  check = false;
  constructor(
    public userService: UserService,
  ) {}

  createUser() {
    this.check = false
    const name = this.nameInput.nativeElement.value;
    const email = this.emailInput.nativeElement.value;
    const active = this.checkInput.nativeElement;
    const role = this.roleInput.nativeElement.value;

    if (name === '' || email === '') {
      alert('Заполните пустые поля!')
    } else {
      if (active.checked) {
        this.check = true;
      }
      const user = new User(name,email,this.check,role);
      this.userService.users.push(user);
    }

  }

}
