import { Component, Input, OnInit } from '@angular/core';
import { User } from '../../shared/user.model';
import { GroupService } from '../../shared/group.service';
import { Group } from '../../shared/group.model';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit{
  @Input() user!: User;
  groupsArray!: Group[];

  constructor(private groupService: GroupService) {}

  ngOnInit() {
    this.groupsArray = this.groupService.getGroups();
    this.groupService.groupsChange.subscribe((groups: Group[]) => {
      this.groupsArray = groups;
    })
  }

  addUsers() {
    this.groupsArray.forEach(currentGroup => {
      if (currentGroup.value) {
        if (!(currentGroup.userArray.includes(this.user))) {
          currentGroup.userArray.push(this.user);
        }
      }
    })
  }

  activeCheck() {
    if (this.user.active) {
      return 'yes';
    } else {
      return 'no'
    }
  }


}
